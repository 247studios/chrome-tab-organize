# chrome-tab-organize

Chrome extension that helps organize your tabs.

![Demo](./demo.gif)

# Download

https://chrome.google.com/webstore/detail/kdffpfecfjnlgoognoknaaeflnidkmdd
