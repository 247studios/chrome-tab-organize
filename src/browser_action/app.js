function onLoad() {
  document.getElementById('form').addEventListener('submit', onFormSubmit);
  document.getElementById('form--title').addEventListener('input', onFormTitleInput);
  chrome.tabs.query({
    currentWindow: true,
  }, (tabs) => {
    render({
      tabs,
    });
  });
}

function onFormTitleInput(event) {
  render({
    title: event.target.value,
  });
}

function onFormSubmit(event) {
  event.preventDefault();
  const formData = new FormData(event.target);
  const title = formData.get('title');
  chrome.tabs.query({
    currentWindow: true,
  }, (tabs) => {
    const tabElements = tabs.map((tab) => ({ tab, el: document.getElementById(`tab-${tab.id}`) }));
    const unmutedTabs = tabElements.filter(({ el }) => el && el.dataset.muted === undefined);
    const mutedTabs = tabElements.filter(({ el }) => el && el.dataset.muted !== undefined);
    if (unmutedTabs.length === 0) {
      return;
    }
    chrome.bookmarks.create({
      title,
    }, (folder) => {
      unmutedTabs.forEach(({ tab, el }) => {
        chrome.bookmarks.create({
          parentId: folder.id,
          title: tab.title,
          url: tab.url,
        });
      });
      render({
        title: '',
        tabs: mutedTabs.map(({ tab }) => tab),
      });
    });
  });
}

function render({
  title,
  tabs,
}) {
  if (title !== undefined) {
    const folder = document.getElementById('folder');
    folder.innerText = title;
    const folderInput = document.getElementById('form--title');
    if (folderInput.value !== title) {
      folderInput.value = title;
    }
  }

  if (tabs !== undefined) {
    const bookmarks = document.getElementById('bookmarks');
    
    while (bookmarks.lastChild) {
      bookmarks.removeChild(bookmarks.lastChild);
    }

    tabs.map(tab => {
      const item = document.createElement('button');
      item.id = `tab-${tab.id}`;
      item.className = 'bookmark-item';

      item.addEventListener('click', () => {
        const isMuted = item.dataset.muted !== undefined;
        toggle.style.backgroundImage = isMuted
          ? `url(assets/folder-minus-light.svg)`
          : `url(assets/folder-plus-light.svg)`;
        if (isMuted) {
          delete item.dataset.muted;
        } else {
          item.dataset.muted = '';
        }
      });
      
      const toggle = document.createElement('div');
      toggle.className = 'bookmark-item--toggle bookmark-item--icon';
      toggle.style.backgroundImage = `url(assets/folder-minus-light.svg)`;
      item.appendChild(toggle);
      
      const icon = document.createElement('div');
      icon.className = 'bookmark-item--icon bookmark-item--favicon';
      icon.style.backgroundImage = `url(${tab.favIconUrl || 'assets/globe-light.svg'})`;
      item.appendChild(icon);
      
      const title = document.createElement('div');
      title.className = 'bookmark-item--title';
      title.innerText = tab.title;
      item.appendChild(title);
      
      return item;
    }).forEach((el) => bookmarks.appendChild(el));

    if (tabs.length === 0) {
      const done = document.createElement('input');
      done.type = 'button';
      done.className = 'btn btn-primary';
      done.style.marginLeft = '1rem';
      done.value = 'Drop The Burden';
      done.addEventListener('click', () => {
        chrome.tabs.query({
          currentWindow: true,
        }, (tabs) => {
          tabs.forEach(tab => chrome.tabs.remove(tab.id));
          chrome.tabs.create({ url: `chrome://bookmarks` });
        });
      });
      bookmarks.appendChild(done);
      document.getElementById('form').remove();
      document.getElementById('bookmark-folder').remove();
    }
  }
}

window.addEventListener('load', onLoad);
